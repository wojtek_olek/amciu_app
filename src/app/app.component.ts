import { Component } from '@angular/core';
import { DataStorageService } from './services/data-storage.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html'
})
export class AppComponent {
  title: String = 'Orders';

  constructor(private dataStorageService: DataStorageService) {}

  changeTitle(title: String) {
    this.title = title;
  }

  ngAfterContentInit() {
    this.dataStorageService.getOrders();
    this.dataStorageService.getOrdersHistory();
  }
}
