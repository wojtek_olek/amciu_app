import { Injectable } from "@angular/core";
import { Http, Response } from "@angular/http";
import { OrdersService } from "./orders.service";
import { Order } from "../shared/models/order.model";
import "rxjs/add/operator/map";

@Injectable()
export class DataStorageService {
  constructor(private http: Http, private ordersService: OrdersService) {}

  storeOrders() {
    this.http
      .put(
        "https://monte-task.firebaseio.com/orders.json",
        this.ordersService.getOrders()
      )
      .subscribe();
  }

  storeHistory() {
    this.http
      .put(
        "https://monte-task.firebaseio.com/history.json",
        this.ordersService.getOrdersHistory()
      )
      .subscribe();
  }

  getOrders() {
    this.http
      .get("https://monte-task.firebaseio.com/orders.json")
      .map((response: Response) => {
        const orders: Order[] = response.json();
        for (let order of orders) {
          if (!order["meals"]) {
            order["meals"] = [];
          }
        }
        return orders;
      })
      .subscribe((orders: Order[]) => {
        this.ordersService.setOrders(orders);
      });
  }

  getOrdersHistory() {
    this.http
      .get("https://monte-task.firebaseio.com/history.json")
      .map((response: Response) => {
        let ordersHistory: Order[] = response.json();
        if (!ordersHistory) {
          ordersHistory = [];
        }
        return ordersHistory;
      })
      .subscribe((ordersHistory: Order[]) => {
        this.ordersService.setOrdersHistory(ordersHistory);
      });
  }
}
