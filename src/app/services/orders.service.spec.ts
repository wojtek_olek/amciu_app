import { TestBed, inject, getTestBed } from "@angular/core/testing";
import { OrdersService } from "./orders.service";
import { Order } from "./../shared/models/order.model";
import { Meal } from "./../shared/models/meal.model";

let ordersMock: Order[] = [
  new Order("4jelenie", "Michael Travolta", 40, "opened", [
    new Meal("Zestaw obiadowy z surowka", "Michael Travolta", 20),
    new Meal("Zestaw obiadowy z surowka", "Seba Kartezjusz", 20),
    new Meal("Zestaw obiadowy z surowka", "Zdzichu Pompa", 20)
  ]),
  new Order("Zupa", "Johny Kowalsky", 60, "opened", [
    new Meal("Zestaw obiadowy z surowka", "Michael Travolta", 20),
    new Meal("Zestaw obiadowy z surowka", "Seba Kartezjusz", 20),
    new Meal("Zestaw obiadowy z surowka", "Zdzichu Pompa", 20)
  ])
];

describe("OrdersService", () => {
  let ordersService: OrdersService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [OrdersService]
    });
    const testbed = getTestBed();
    ordersService = testbed.get(OrdersService);
  });

  it("Should be created", () => {
    expect(ordersService).toBeTruthy();
  });

  it("Should add meal", () => {
    let calculateSpy = spyOn(ordersService, "calculatePrize");
    ordersService["orders"] = ordersMock;
    const index = 0;
    let meal = {
      name: "test",
      orderer: "test",
      prize: 10
    };
    ordersService.addMeal(index, meal);
    expect(calculateSpy).toHaveBeenCalledWith(index);
  });

  it("Should deleate meal", () => {
    let calculateSpy = spyOn(ordersService, "calculatePrize");
    ordersService["orders"] = ordersMock;
    const index = 0;
    let mealIndex = 0;
    ordersService.deleateMeal(index, mealIndex);
    expect(calculateSpy).toHaveBeenCalledWith(index);
  });

  it("Should calculate price", () => {
    ordersService["orders"] = ordersMock;
    const index = 0;
    ordersService.calculatePrize(index);
    let orderMock = ordersService["orders"];
    let price = orderMock[index].prize;
    expect(price).toEqual(50);
  });

  it("Should set orders", () => {
    ordersService["orders"] = [];
    let ordersMock2 = [
      new Order("4jelenie", "Michael Travolta", 30, "opened", [
        new Meal("Zestaw obiadowy z surowka", "Michael Travolta", 10),
        new Meal("Zestaw obiadowy z surowka", "Seba Kartezjusz", 10),
        new Meal("Zestaw obiadowy z surowka", "Zdzichu Pompa", 10)
      ])
    ];
    ordersService.setOrders(ordersMock2);
    let orderMock = ordersService["orders"];
    expect(orderMock).toEqual(ordersMock2);
  });

  it("Should set ordersHistory", () => {
    let ordersMock2 = [
      new Order("4jelenie", "Michael Travolta", 30, "opened", [
        new Meal("Zestaw obiadowy z surowka", "Michael Travolta", 10),
        new Meal("Zestaw obiadowy z surowka", "Seba Kartezjusz", 10),
        new Meal("Zestaw obiadowy z surowka", "Zdzichu Pompa", 10)
      ])
    ];
    ordersService.setOrdersHistory(ordersMock2);
    let ordersHistoryMock = ordersService["ordersHistory"];
    expect(ordersHistoryMock).toEqual(ordersMock2);
  });

  it("Should add order", () => {
    ordersService["orders"] = [];
    let ordersMock2 = new Order("4jelenie", "Michael Travolta", 30, "opened", [
      new Meal("Zestaw obiadowy z surowka", "Michael Travolta", 10),
      new Meal("Zestaw obiadowy z surowka", "Seba Kartezjusz", 10),
      new Meal("Zestaw obiadowy z surowka", "Zdzichu Pompa", 10)
    ]);
    ordersService.addOrder(ordersMock2);
    let ordersMock = ordersService["orders"];
    expect(ordersMock).toEqual([ordersMock2]);
  });

  it("Should delivered the order", () => {
    ordersService["ordersHistory"] = [];
    let ordersMock2 = new Order("4jelenie", "Michael Travolta", 30, "opened", [
      new Meal("Zestaw obiadowy z surowka", "Michael Travolta", 10),
      new Meal("Zestaw obiadowy z surowka", "Seba Kartezjusz", 10),
      new Meal("Zestaw obiadowy z surowka", "Zdzichu Pompa", 10)
    ]);
    ordersService["orders"] = [ ordersMock2 ];
    const index = 0;
    let ordersMock = ordersService["orders"];
    ordersService.orderDelivered(index);
    expect(ordersMock).toEqual([]);
  });

  it("Should set the status of the first order", () => {
    ordersService["ordersHistory"] = [];
    let ordersMock2 = new Order("4jelenie", "Michael Travolta", 30, "opened", [
      new Meal("Zestaw obiadowy z surowka", "Michael Travolta", 10),
      new Meal("Zestaw obiadowy z surowka", "Seba Kartezjusz", 10),
      new Meal("Zestaw obiadowy z surowka", "Zdzichu Pompa", 10)
    ]);
    ordersService["orders"] = [ ordersMock2 ];
    const index = 0;
    const status = 'ordered';
    ordersService.statusChange(index, status);
    let ordersMock = ordersService["orders"];
    let statusMock = ordersMock[index].status;
    expect(statusMock).toEqual('ordered');
  });
});
