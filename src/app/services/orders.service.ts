import { Injectable }     from '@angular/core';
import { Order }          from './../shared/models/order.model';
import { Meal }           from './../shared/models/meal.model';
import { Subject }        from 'rxjs/Subject';

@Injectable()
export class OrdersService {

  orderChanged = new Subject<Order[]>();

  private orders: Order[] = [];
  private ordersHistory: Order[] = [];

  constructor() { }

  calculatePrice(index) {
    let sum = 0;
    for (let i = 0; i < this.orders[index].meals.length; i++) {
      sum += this.orders[index].meals[i].price;
    }
    this.orders[index].price = sum;
  }

  setOrders(orders: Order[]) {
    this.orders = orders;
    this.orderChanged.next(this.orders.slice());
  }

  setOrdersHistory(orders: Order[]) {
    this.ordersHistory = orders;
  }

  getOrders() {
    return this.orders.slice();
  }

  getOrdersHistory() {
    return this.ordersHistory.slice();
  }

  getOrder(index) {
    return this.orders[index];
  }

  addOrder(order: Order) {
    this.orders.push(order);
    this.orderChanged.next(this.orders.slice());
    
  }

  addMeal(index, meal: Meal) {
    this.orders[index].meals.push(meal);
    this.calculatePrice(index);
  }

  deleateMeal(index, mealIndex) {
    this.orders[index].meals.splice(mealIndex, 1);
    this.calculatePrice(index);
  }

  orderDelivered(index) {
    let finalized = this.orders[index];
    finalized.status = 'delivered';
    this.ordersHistory.push(finalized);
    this.orders.splice(index, 1);
    this.orderChanged.next(this.orders.slice());
  }

  statusChange(index, status) {
    this.orders[index].status = status;
  }

}
