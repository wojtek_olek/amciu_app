import { NgModule }                   from '@angular/core';
import { Routes, RouterModule }       from '@angular/router';
// Components
import { OrdersComponent }            from './components/orders/orders.component';
import { HistoryComponent }           from './components/history/history.component';

const routes: Routes = [
  { path: '', redirectTo: '/orders', pathMatch: 'full' },
  { path: 'orders', component: OrdersComponent },
  { path: 'history', component: HistoryComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule { }
