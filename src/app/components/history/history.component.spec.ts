import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HistoryComponent } from './history.component';
import { OrdersService } from '../../services/orders.service';
import { Order } from '../../shared/models/order.model';

let orderServiceStub = {
  getOrdersHistory() {
    return []
  }
}

describe('HistoryComponent', () => {
  let component: HistoryComponent;
  let fixture: ComponentFixture<HistoryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [],
declarations: [ HistoryComponent ],
      providers: [
        { provide: OrdersService, useValue: orderServiceStub }
      ]
    })
    .overrideTemplate(HistoryComponent ,'<span></span>')
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HistoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('Should create component', () => {
    expect(component).toBeTruthy();
  });

  it('Should get ordersHistory', () => {
    const history: Order[] = [];
    component.ngOnInit();
    expect(component.ordersHist).toEqual(history);
  });
});
