import { Component, OnInit } from '@angular/core';
import { OrdersService } from '../../services/orders.service';
import { Order } from './../../shared/models/order.model';

@Component({
  selector: 'app-history',
  templateUrl: './history.component.html'
})
export class HistoryComponent implements OnInit {

  ordersHist: Order[];

  constructor(private ordersService: OrdersService) { }

  ngOnInit() {
    this.ordersHist = this.ordersService.getOrdersHistory();
  }

}
