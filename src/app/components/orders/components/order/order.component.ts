import { Component, OnInit, Input, TemplateRef, HostListener } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { Order } from "../../../../shared/models/order.model";
import { BsModalService } from "ngx-bootstrap/modal";
import { BsModalRef } from "ngx-bootstrap/modal/bs-modal-ref.service";
import { OrdersService } from '../../../../services/orders.service';
import { DataStorageService } from '../../../../services/data-storage.service';

@Component({
  selector: "app-order",
  templateUrl: "./order.component.html"
})
export class OrderComponent implements OnInit {
  @Input() order: Order;
  @Input() index;

  advancedOpened = false;
  modalRef: BsModalRef;
  newMealForm: FormGroup;
  oOpened = true;
  oFinalized = false;
  oOrdered = false;
  oDelivered = false;

  constructor(
    private modalService: BsModalService,
    private formBuilder: FormBuilder,
    private orderService: OrdersService,
    private dataStorageService: DataStorageService
  ) {
    this.createMealForm();
  }

  ngOnInit() {
    if (this.order.status === 'delivered') {
      this.oDelivered = true;
      this.oOpened = false;
    } else if (this.order.status === 'finalized') {
      this.oFinalized = true;
      this.oOpened = false;
    } else if (this.order.status === 'ordered') {
      this.oOrdered = true;
      this.oOpened = false;
    }
  }

  toggleAdvanced() {
    this.advancedOpened = !this.advancedOpened;
  }

  openModal(template: TemplateRef<any>) {
    this.modalRef = this.modalService.show(template);
  }

  createMealForm() {
    this.newMealForm = this.formBuilder.group({
      meal: ['', Validators.compose([
        Validators.required,
        Validators.minLength(3),
        Validators.maxLength(30),
      ])],
      orderer: ['', Validators.compose([
        Validators.required,
        Validators.minLength(3),
        Validators.maxLength(15)
      ])],
      price: ['', Validators.required]
    });
  }

  addNewMeal() {
    let meal = {
      name: this.newMealForm.get('meal').value,
      orderer: this.newMealForm.get('orderer').value,
      price: this.newMealForm.get('price').value
    }

    this.orderService.addMeal(this.index, meal);
    this.modalRef.hide();
    this.createMealForm();
    this.dataStorageService.storeOrders();
  }

  orderFinalized(index) {
    this.oFinalized = true;
    this.oOpened = false;
    const status = 'finalized';
    this.orderService.statusChange(index, status);
    this.dataStorageService.storeOrders();
  }

  orderDelivered(index) {
    this.oDelivered = true;
    this.orderService.orderDelivered(index);
    this.dataStorageService.storeOrders();
    this.dataStorageService.storeHistory();
  }

  orderOrdered(index) {
    this.oOrdered = true;
    this.oFinalized = false;
    this.oOpened = false;
    const status = 'ordered';
    this.orderService.statusChange(index, status);
    this.dataStorageService.storeOrders();
  }
}
