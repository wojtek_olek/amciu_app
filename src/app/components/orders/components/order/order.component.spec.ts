import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { OrderComponent } from './order.component';
import { OrdersService } from '../../../../services/orders.service';
import { DataStorageService } from '../../../../services/data-storage.service';
import { BsModalService } from "ngx-bootstrap/modal";
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';
import { FormBuilder } from '@angular/forms';
import { Order } from '../../../../shared/models/order.model';
import { Meal } from '../../../../shared/models/meal.model';

let ordersServiceStub = {
  addMeal() {},

  statusChange() {},

  orderDelivered() {}
}

let dataStorageServiceStub = {
  storeOrders() {},

  storeHistory() {}
}

let bsModalServiceStub = {
  show () {}
}

let orderMock = new Order(
  '4jelenie',
  'Michael Travolta',
  60,
  'opened',
  [
    new Meal(
      'Zestaw obiadowy z surowka',
      'Michael Travolta',
      20
    ),
    new Meal(
      'Zestaw obiadowy z surowka',
      'Seba Kartezjusz',
      20
    ),
    new Meal(
      'Zestaw obiadowy z surowka',
      'Zdzichu Pompa',
      20
    )
  ]
)

let modalRefMock = {
  hide () {}
}

describe('OrderComponent', () => {
  let component: OrderComponent;
  let fixture: ComponentFixture<OrderComponent>;
  let orderService: OrdersService;
  let dataStorageService: DataStorageService;
  let modalService: BsModalService;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [],
      declarations: [ OrderComponent ],
      providers: [
        { provide: OrdersService, useValue: ordersServiceStub },
        { provide: DataStorageService, useValue: dataStorageServiceStub },
        { provide: BsModalService, useValue: bsModalServiceStub },
        FormBuilder
      ]
    })
    .overrideTemplate(OrderComponent ,'<span></span>')
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OrderComponent);
    component = fixture.componentInstance;
    orderService = fixture.debugElement.injector.get(OrdersService);
    dataStorageService = fixture.debugElement.injector.get(DataStorageService);
    modalService = fixture.debugElement.injector.get(BsModalService);
    component.order = orderMock;
    fixture.detectChanges();
    
  });

  it('Should create component', () => {
    expect(component).toBeTruthy();
  });

  it('Should set oDelivered to true and oOpened to false', () => {
    component.order.status = 'delivered';
    component.ngOnInit();
    expect(component.oDelivered).toBeTruthy();
    expect(component.oOpened).toBeFalsy();
  });

  it('Should set oFinalized to true and oOpened to false', () => {
    component.order.status = 'finalized';
    component.ngOnInit();
    expect(component.oFinalized).toBeTruthy();
    expect(component.oOpened).toBeFalsy();
  });

  it('Should set oOrdered to true and oOpened to false', () => {
    component.order.status = 'ordered';
    component.ngOnInit();
    expect(component.oOrdered).toBeTruthy();
    expect(component.oOpened).toBeFalsy();
  });

  it('Should toggle advanced view', () => {
    component.advancedOpened = false;
    component.toggleAdvanced();
    expect(component.advancedOpened).toBeTruthy();
  });

  it('Should add new meal', () => {
    let addMealSpy = spyOn(orderService, 'addMeal');
    let storeOrdersSpy = spyOn(dataStorageService, 'storeOrders');
    let createMealFormSpy = spyOn(component, 'createMealForm');
    let mealMock = {
      name: '',
      orderer: '',
      prize: ''
    }
    let index = 0;
    component.index = index;
    component.modalRef = modalRefMock;
    component.addNewMeal['meal'] = mealMock; 
    component.addNewMeal();
    expect(addMealSpy).toHaveBeenCalledWith(index, mealMock);
    expect(createMealFormSpy).toHaveBeenCalled();
    expect(storeOrdersSpy).toHaveBeenCalled();
  });

  it('Should finalized the order', () => {
    let statusChangeSpy = spyOn(orderService, 'statusChange');
    let storeOrderSpy = spyOn(dataStorageService, 'storeOrders');
    const index = 0;
    component.orderFinalized(index);
    expect(component.oFinalized).toBeTruthy();
    expect(component.oOpened).toBeFalsy();
    expect(statusChangeSpy).toHaveBeenCalled();
    expect(storeOrderSpy).toHaveBeenCalled();
  });

  it('Should delivered the order', () => {
    let orderDeliveredSpy = spyOn(orderService, 'orderDelivered');
    let storeOrderSpy = spyOn(dataStorageService, 'storeOrders');
    let storeHistorySpy = spyOn(dataStorageService, 'storeHistory');
    const index = 0;
    component.orderDelivered(index);
    expect(component.oDelivered).toBeTruthy();
    expect(orderDeliveredSpy).toHaveBeenCalled();
    expect(storeHistorySpy).toHaveBeenCalled();
    expect(storeOrderSpy).toHaveBeenCalled();
  });

  it('Should ordered the order', () => {
    let statusChangeSpy = spyOn(orderService, 'statusChange');
    let storeOrderSpy = spyOn(dataStorageService, 'storeOrders');
    const index = 0;
    component.orderOrdered(index);
    expect(component.oOrdered).toBeTruthy();
    expect(component.oFinalized).toBeFalsy();
    expect(component.oOpened).toBeFalsy();
    expect(statusChangeSpy).toHaveBeenCalled();
    expect(storeOrderSpy).toHaveBeenCalled();
  });

});
