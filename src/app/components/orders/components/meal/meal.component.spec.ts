import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { MealComponent } from './meal.component';
import { OrdersService } from '../../../../services/orders.service';
import { DataStorageService } from '../../../../services/data-storage.service';

let ordersServiceStub = {
  deleateMeal() {}
}

let dataStorageServiceStub = {
  storeOrders() {}
}

describe('MealComponent', () => {
  let component: MealComponent;
  let fixture: ComponentFixture<MealComponent>;
  let orderService: OrdersService;
  let dataStorageService: DataStorageService;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [],  
      declarations: [ MealComponent ],
      providers: [
        { provide: OrdersService, useValue: ordersServiceStub },
        { provide: DataStorageService, useValue: dataStorageServiceStub }
      ]
    })
    .overrideTemplate(MealComponent ,'<span></span>')
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MealComponent);
    component = fixture.componentInstance;
    orderService = fixture.debugElement.injector.get(OrdersService);
    dataStorageService = fixture.debugElement.injector.get(DataStorageService);
    fixture.detectChanges();
  });

  it('Should create component', () => {
    expect(component).toBeTruthy();
  });

  it('Should open mobileMenu', () => {
    component.mobileOrderMenu = false;
    const event = new Event('click');
    component.openMobileMenu(event);
    expect(component.mobileOrderMenu).toBeTruthy();
  });

  it('Should close mobileMenu', () => {
    component.mobileOrderMenu = true;
    const event = new Event('click');
    component.clickedOutside(event)
    expect(component.mobileOrderMenu).toBeFalsy();
  });

  it('Should have called deleateMeal and storeOrders', () => {
    let deleateSpy = spyOn(orderService, 'deleateMeal');
    let storeSpy = spyOn(dataStorageService, 'storeOrders');
    component.onDeleateMeal(0);
    expect(deleateSpy).toHaveBeenCalled();
    expect(storeSpy).toHaveBeenCalled();
  });
});
