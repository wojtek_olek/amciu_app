import { Component, OnInit, 
  Input, HostListener }             from '@angular/core';
import { Meal }                     from '../../../../shared/models/meal.model';
import { OrdersService }            from '../../../../services/orders.service';
import { DataStorageService }       from '../../../../services/data-storage.service';

@Component({
  selector: 'app-meal',
  templateUrl: './meal.component.html'
})
export class MealComponent implements OnInit {

  @Input() meal: Meal;
  @Input() index;
  @Input() orderIndex;
  @Input() orderFinalized;
  @Input() orderOrdered;
  @Input() orderDelivered;

  mobileOrderMenu = false;

  constructor(private orderService: OrdersService,
              private dataStorageService: DataStorageService) { }

  ngOnInit() {
  }

  openMobileMenu($event: Event){
    $event.preventDefault();
    $event.stopPropagation();
    this.mobileOrderMenu = !this.mobileOrderMenu;
  }
  @HostListener('document:click', ['$event']) clickedOutside($event){
    this.mobileOrderMenu = false;
  }

  onDeleateMeal(mealIndex) {
    this.orderService.deleateMeal(this.orderIndex, mealIndex);
    this.dataStorageService.storeOrders();
  }
}
