import { Component, OnInit, TemplateRef }     from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { OrdersService }                      from "../../services/orders.service";
import { Order }                              from "../../shared/models/order.model";
import { BsModalService }                     from "ngx-bootstrap/modal";
import { BsModalRef }                         from "ngx-bootstrap/modal/bs-modal-ref.service";
import { DataStorageService }                 from '../../services/data-storage.service';

@Component({
  selector: "app-orders",
  templateUrl: "./orders.component.html"
})
export class OrdersComponent implements OnInit {
  orders: Order[];
  modalRef: BsModalRef;
  newOrderForm: FormGroup;

  constructor(
    private ordersService: OrdersService,
    private modalService: BsModalService,
    private formBuilder: FormBuilder,
    private dataStorageService: DataStorageService
  ) {
    this.createOrderForm();
  }

  ngOnInit() {
    this.orders = this.ordersService.getOrders();
    this.ordersService.orderChanged.subscribe((orders: Order[]) => {
      this.orders = orders;
    })
  }

  openModal(template: TemplateRef<any>) {
    this.modalRef = this.modalService.show(template);
  }

  createOrderForm() {
    this.newOrderForm = this.formBuilder.group(
      {
        restaurant: ['', Validators.required],
        owner: ['', Validators.compose([
          Validators.required,
          Validators.minLength(3),
          Validators.maxLength(15)
        ])]
      }
    )
  }

  addNewOrder() {
    let order = new Order(
      this.newOrderForm.get('restaurant').value,
      this.newOrderForm.get('owner').value,
      0,
      'opened',
      []
    )
    this.ordersService.addOrder(order);
    this.modalRef.hide();
    this.createOrderForm();
    this.dataStorageService.storeOrders();
  }
}
