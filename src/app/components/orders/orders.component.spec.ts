import {
  async,
  ComponentFixture,
  TestBed,
  inject
} from "@angular/core/testing";
import { OrdersService } from "../../services/orders.service";
import { OrdersComponent } from "./orders.component";
import { BsModalService } from "ngx-bootstrap/modal";
import { BsModalRef } from "ngx-bootstrap/modal/bs-modal-ref.service";
import { FormBuilder } from "@angular/forms";
import { DataStorageService } from "../../services/data-storage.service";
import { Observable } from "rxjs/Observable";
import "rxjs/add/observable/of";
import { Subject } from "rxjs/Subject";
import { Order } from "../../shared/models/order.model";

let ordersServiceStub = {
  getOrders() {},

  addOrder() {},

  orderChanged: new Subject<any>()
};

let dataStorageServiceStub = {
  storeOrders() {}
};

let bsModalServiceStub = {
  show() {}
};
let modalRefMock = {
  hide() {}
};

describe("OrdersComponent", () => {
  let component: OrdersComponent;
  let fixture: ComponentFixture<OrdersComponent>;
  let ordersService: OrdersService;
  let dataStorageService: DataStorageService;
  let modalService: BsModalService;

  beforeEach(
    async(() => {
      TestBed.configureTestingModule({
        imports: [],
        declarations: [OrdersComponent],
        providers: [
          { provide: OrdersService, useValue: ordersServiceStub },
          { provide: DataStorageService, useValue: dataStorageServiceStub },
          { provide: BsModalService, useValue: bsModalServiceStub },
          FormBuilder
        ]
      })
        .overrideTemplate(OrdersComponent, "<span></span>")
        .compileComponents();
    })
  );

  beforeEach(() => {
    fixture = TestBed.createComponent(OrdersComponent);
    component = fixture.componentInstance;
    ordersService = fixture.debugElement.injector.get(OrdersService);
    dataStorageService = fixture.debugElement.injector.get(DataStorageService);
    modalService = fixture.debugElement.injector.get(BsModalService);
    fixture.detectChanges();
  });

  it('Should create component', () => {
    expect(component).toBeTruthy();
  });

  it('Should add order', () => {
    let addOrderSpy = spyOn(ordersService, 'addOrder');
    let createOrderFormSpy = spyOn(component, 'createOrderForm');
    let dataStorageSpy = spyOn(dataStorageService, 'storeOrders');
    let orderMock = new Order(
      '',
      '',
      0,
      'opened',
      []
    );
    component.modalRef = modalRefMock;
    component.addNewOrder();
    expect(addOrderSpy).toHaveBeenCalledWith(orderMock);
    expect(createOrderFormSpy).toHaveBeenCalled();
  });
});
