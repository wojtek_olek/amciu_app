import { BrowserModule }        from '@angular/platform-browser';
import { NgModule }             from '@angular/core';
import { FormsModule, 
  ReactiveFormsModule }         from '@angular/forms';
import { HttpModule }           from '@angular/http';

import { AppComponent }         from './app.component';
// Router
import { AppRoutingModule }     from './app-routing.module';
// Components
import { HeaderComponent }      from './components/header/header.component';
import { OrdersComponent }      from './components/orders/orders.component';
import { HistoryComponent }     from './components/history/history.component';
import { FooterComponent }      from './components/footer/footer.component';
import { OrderComponent }       from './components/orders/components/order/order.component';

// Services
import { OrdersService }        from './services/orders.service';
import { DataStorageService }   from './services/data-storage.service';

// Bootstrap
import { ModalModule }          from 'ngx-bootstrap/modal';
import { MealComponent } from './components/orders/components/meal/meal.component';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    OrdersComponent,
    HistoryComponent,
    FooterComponent,
    OrderComponent,
    MealComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ModalModule.forRoot(),
    FormsModule,
    ReactiveFormsModule,
    HttpModule
  ],
  providers: [OrdersService, DataStorageService],
  bootstrap: [AppComponent]
})
export class AppModule { }
