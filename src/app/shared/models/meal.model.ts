export class Meal {
  public name: string;
  public orderer: string;
  public price: number;

  constructor(name: string, orderer: string, price: number) {
    this.name = name;
    this.orderer = orderer;
    this.price = price;
  }
}