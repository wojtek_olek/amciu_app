import { Meal } from './meal.model';

export class Order {
  public supplierName: string;
  public owner: string;
  public price: number;
  public status: string;
  public meals: Meal[];

  constructor(supplierName: string, owner: string, price: number, status: string, meals: Meal[]) {
    this.supplierName = supplierName;
    this.owner = owner;
    this.price = price;
    this.status = status;
    this.meals = meals;
  }
}